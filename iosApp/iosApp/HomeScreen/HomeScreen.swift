//
//  HomeScreen.swift
//  iosApp
//
//  Created by Calvin Macintosh on 4/3/23.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct HomeScreen: View {
    var body: some View {
        VStack {
            Text("ShopSync")
            Spacer()
        }
    }
}

struct HomeScreen_Previews: PreviewProvider {
    static var previews: some View {
        HomeScreen()
    }
}
