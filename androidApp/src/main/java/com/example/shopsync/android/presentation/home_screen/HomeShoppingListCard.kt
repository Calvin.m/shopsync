package com.example.shopsync.android.presentation.home_screen

import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.shopsync.android.presentation.HomeScreenViewModel
import com.example.shopsync.service.ShoppingList

@Composable
fun HomeShoppingListCard(
    shoppingList: ShoppingList,
    homeScreenViewModel: HomeScreenViewModel
) {
    Card(
        backgroundColor = MaterialTheme.colors.background,
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 4.dp)
            .clickable {
                Log.e("card clicked:", "$shoppingList")
                homeScreenViewModel.selectList(shoppingList)
            },
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp
    ) {
        Row() {
            Text(
                text = shoppingList.name,
                modifier = Modifier.padding(16.dp),
                style = MaterialTheme.typography.body1,
                color = if (shoppingList.name == "untitled") Color.Gray else MaterialTheme.colors.onBackground
            )
            Spacer(Modifier.weight(1f))
            IconButton(onClick = {}) {
                Icon(
                    imageVector = Icons.Default.MoreVert,
                    contentDescription = null
                )
            }
        }
    }
}