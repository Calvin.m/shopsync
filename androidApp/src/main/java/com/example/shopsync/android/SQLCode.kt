package com.example.shopsync.android

/*

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE shopping_lists (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE shopping_list_items (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    quantity INTEGER NOT NULL,
    list_id INTEGER NOT NULL,
    FOREIGN KEY (list_id) REFERENCES shopping_lists(id)
);
CREATE TABLE shared_lists (
    id SERIAL PRIMARY KEY,
    user_id INT,
    list_id INT,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (list_id) REFERENCES shopping_lists(id)
);

INSERT INTO public.users (email, password) VALUES ('alice@example.com', 'alicePassword');
INSERT INTO public.users (email, password) VALUES ('bob@example.com', 'bobPassword');

INSERT INTO public.shopping_lists (name, user_id) VALUES ('Groceries', 1);
INSERT INTO public.shopping_lists (name, user_id) VALUES ('Electronics', 2);

INSERT INTO public.shopping_list_items (name, quantity, list_id) VALUES ('Milk', 2, 1);
INSERT INTO public.shopping_list_items (name, quantity, list_id) VALUES ('Eggs', 12, 1);
INSERT INTO public.shopping_list_items (name, quantity, list_id) VALUES ('HDMI Cable', 1, 2);
INSERT INTO public.shopping_list_items (name, quantity, list_id) VALUES ('USB Cable', 2, 2);



*/