package com.example.shopsync.android.di

import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow

class LoggedInEvent {
    private val _event = MutableSharedFlow<UserState>()
    val event: SharedFlow<UserState> get() = _event

    suspend fun emit(token: String, email: String) {
        _event.emit(UserState(token, email))
    }
}

data class UserState(
    val token: String,
    val email: String
)