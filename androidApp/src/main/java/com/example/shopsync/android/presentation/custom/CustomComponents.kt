//package com.example.shopsync.android.presentation.custom
//
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.Column
//import androidx.compose.foundation.layout.fillMaxSize
//import androidx.compose.material.*
//import androidx.compose.material.icons.Icons
//import androidx.compose.material.icons.filled.Add
//import androidx.compose.runtime.Composable
//import androidx.compose.ui.Modifier
//import com.example.shopsync.android.presentation.SearchBar
//import com.example.shopsync.android.presentation.ShoppingLists
//
//@Composable
//fun CustomTopAppBar(title: @Composable () -> Unit) {
//    TopAppBar(
//        title = title,
//        backgroundColor = MaterialTheme.colors.secondary,
//        contentColor = MaterialTheme.colors.onSecondary
//    )
//}
//
//@Composable
//fun CustomScaffold(
//    modifier: Modifier = Modifier,
//    topBar: @Composable () -> Unit = {},
//    floatingActionButton: @Composable () -> Unit = {},
//    floatingActionButtonPosition: FabPosition = FabPosition.End,
//    content: @Composable () -> Unit
//) {
//    Surface(
//        modifier = modifier.fillMaxSize(),
//        color = MaterialTheme.colors.primary
//    ) {
//        Column {
//            topBar()
//            content()
//        }
//
//        Box(
//            modifier = Modifier.fillMaxSize(),
//            contentAlignment = floatingActionButtonPosition.align
//        ) {
//            floatingActionButton()
//        }
//    }
//}
//
//@Composable
//fun HomeScreen() {
//    CustomScaffold(
//        topBar = {
//            CustomTopAppBar(title = { Text("ShopSync") })
//        },
//        floatingActionButton = {
//            FloatingActionButton(onClick = { /* Add logic to create a new shopping list */ }) {
//                Icon(Icons.Default.Add, contentDescription = "Add Shopping List")
//            }
//        },
//        content = {
//            Column {
//                SearchBar()
//                ShoppingLists(/* Pass the state object here */)
//            }
//        }
//    )
//}
