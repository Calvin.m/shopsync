//package com.example.shopsync.android.navigation
//
//import android.util.Log
//import androidx.compose.runtime.Composable
//import androidx.compose.ui.platform.LocalContext
//import androidx.core.app.ComponentActivity
//import androidx.hilt.navigation.compose.hiltViewModel
//import androidx.navigation.NavHostController
//import androidx.navigation.NavType
//import androidx.navigation.compose.NavHost
//import androidx.navigation.compose.composable
//import androidx.navigation.navArgument
//import com.example.shopsync.android.MyApplicationTheme
//import com.example.shopsync.android.presentation.HomeScreen
//import com.example.shopsync.android.presentation.HomeScreenViewModel
//import com.example.shopsync.android.presentation.SplashScreen
//import com.example.shopsync.android.presentation.list_screen.ListScreen
//import com.example.shopsync.android.presentation.list_screen.ListScreenViewModel
//import com.example.shopsync.android.presentation.login_screen.LoginScreen
//import com.example.shopsync.android.presentation.login_screen.LoginViewModel
//import com.example.shopsync.android.presentation.shared.SharedViewModel
//
//@Composable
//fun SetUpNavGraph(
//    navControllerHost: NavHostController,
//    loginViewModel: LoginViewModel,
//    homeScreenViewModel: HomeScreenViewModel,
//    listScreenViewModel: ListScreenViewModel,
//    hasValidToken: Boolean,
//    sharedViewModel: SharedViewModel
//) {
//    NavHost(navController = navControllerHost, startDestination = Screens.SplashScreen.route) {
//        composable(Screens.SplashScreen.route) {
//            SplashScreen(navController = navControllerHost, loginViewModel, hasValidToken)
//        }
//        composable(Screens.LoginScreen.route) {
//            MyApplicationTheme {
//                LoginScreen(navControllerHost, loginViewModel)
//            }
//        }
//        composable(Screens.HomeScreen.route) {
//            val homeScreenViewModel: HomeScreenViewModel = hiltViewModel()
//            val context = LocalContext.current
//            val activity = context as? ComponentActivity
//            activity?.let {
//                MyApplicationTheme {
//                    HomeScreen(navControllerHost, homeScreenViewModel, it)
//                }
//            }
//        }
//        composable(
//            Screens.ListScreen.route,
//            arguments = listOf(
//                navArgument("list_id") { type = NavType.IntType; defaultValue = -1 },
//                navArgument("list_name") {
//                    type = NavType.StringType; defaultValue = "untitled"
//                }
//            )
//        ) { backStackEntry ->
//            val listId = backStackEntry.arguments?.getInt("list_id")
//            Log.e("LIST ID: ", "IN NAV GRAPH: $listId")
//            val listName = backStackEntry.arguments?.getString("list_name")
//            Log.e("LIST NAME: ", "IN NAV GRAPH: $listName")
//            MyApplicationTheme {
//                ListScreen(
//                    onArrowClick = {
//                        navControllerHost.navigateUp()
//                    },
//                    listId,
//                    listName,
//                    listScreenViewModel,
//                    navControllerHost,
//                    homeScreenViewModel,
//                    sharedViewModel
//                )
//            }
//        }
//    }
//}