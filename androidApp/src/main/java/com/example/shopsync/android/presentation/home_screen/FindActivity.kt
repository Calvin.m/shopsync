package com.example.shopsync.android.presentation.home_screen

import android.content.ContextWrapper
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.fragment.app.FragmentActivity

@Composable
fun findActivity(): FragmentActivity? {
    val context = LocalContext.current
    return when {
        context is FragmentActivity -> context
        context is ContextWrapper -> context.baseContext as? FragmentActivity
        else -> null
    }
}
