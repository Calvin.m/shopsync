package com.example.shopsync.android.presentation.login_screen

import android.app.Application
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.*
//import com.example.shopsync.APICall
import com.example.shopsync.android.di.LoggedInEvent
import com.example.shopsync.android.presentation.navigation.NavigationHomeRoute
import com.example.shopsync.android.presentation.navigation.NavigationLoginRoute
import com.example.shopsync.android.presentation.navigation.Navigator
import com.example.shopsync.data.ApiResponse
import com.example.shopsync.data.LoginSignupResponse
import com.example.shopsync.data.repository.ShopSyncRepo
import dagger.hilt.android.lifecycle.HiltViewModel
//import com.example.shopsync.service.LoginSignupResponse
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

//@HiltViewModel // doesn't work
@HiltViewModel
class LoginViewModel @Inject constructor(
    private val application: Application,
    private val loggedInEvent: LoggedInEvent, // Add this parameter
    private val navigator: Navigator
) : AndroidViewModel(application) {
    private val _navigateToHome = MutableStateFlow(false)
    val navigateToHome: StateFlow<Boolean> get() = _navigateToHome

    private val _loginState = MutableStateFlow(LoginState())
    val loginState: StateFlow<LoginState> get() = _loginState

    private val _emailState = MutableStateFlow(UsernameState())
    val emailState: StateFlow<UsernameState> get() = _emailState

    private val _passwordState = MutableStateFlow(PasswordState())
    val passwordState: StateFlow<PasswordState> get() = _passwordState

    private val APICall = ShopSyncRepo()

    fun navigateToHome() {
        viewModelScope.launch {
            navigator.navigate(
                NavigationHomeRoute
            )
        }
    }
    fun navigateToLogin() {
        viewModelScope.launch {
            navigator.navigate(
                NavigationLoginRoute
            )
        }
    }

    fun login(email: String, password: String) {
        viewModelScope.launch {
            val apiResponse: ApiResponse<LoginSignupResponse> = APICall.login(email, password)
            when (apiResponse) {
                is ApiResponse.Success -> {
                    onLoginResponse(apiResponse.data, email)
                    navigator.navigate(NavigationHomeRoute)
                }

                is ApiResponse.Unauthorized -> {
                    Log.e("LOGIN UNAUTHORIZED: ", "LOGIN UNAUTHORIZED")
                    Log.e("email: ", "$email")
                    Log.e("password: ", "$password")
                    // Handle unauthorized case (e.g., navigate to the login screen)
                }

                is ApiResponse.Failure -> {
                    Log.e("LOGIN FAILURE: ", "LOGIN FAILURE")
                    // Handle failure case (e.g., show an error message)
                }

                is ApiResponse.Loading -> {
                    Log.e("LOGIN LOADING: ", "LOGIN LOADING")
                    // You can handle the loading state here if needed
                }
            }
        }
    }

    fun signUp(email: String, password: String) {
        viewModelScope.launch {
            val result: ApiResponse<LoginSignupResponse> = APICall.signup(email, password)

//            onSignupResponse(result)

            when (result) {
                is ApiResponse.Failure -> Log.e("FAILURE:", "LoginViewModel FAILURE")
                ApiResponse.Loading -> Log.e("Loading:", "LoginViewModel Loading")
                is ApiResponse.Success -> {
                    onSignupResponse(result.data, email)
                    navigator.navigate(NavigationHomeRoute)
                }
                ApiResponse.Unauthorized -> Log.e("Unauthorized:", "LoginViewModel Unauthorized")
            }

        }
    }

    private fun saveTokenToSharedPreferences(token: String) {
        val sharedPreferences = getApplication<Application>().getSharedPreferences(
            "ShopSyncPrefs",
            Context.MODE_PRIVATE
        )
        with(sharedPreferences.edit()) {
            putString("jwtToken", token)
            apply()
        }
    }

    private fun saveEmailToSharedPreferences(email: String) {
        val sharedPreferences = getApplication<Application>().getSharedPreferences(
            "ShopSyncPrefs",
            Context.MODE_PRIVATE
        )
        with(sharedPreferences.edit()) {
            putString("email", email)
            apply()
        }
    }

    fun hasValidToken(): Boolean {
        val sharedPreferences = application.getSharedPreferences(
            "ShopSyncPrefs",
            Context.MODE_PRIVATE
        )
        val token = sharedPreferences.getString("jwtToken", null)
        return token != null // return true if the token exists
    }
//    private fun saveUserInfo(email: String) {
//        viewModelScope.launch {
//            _loginState.value = _loginState.value.copy(isLoading = true)
//            val sharedPreferences = getApplication<Application>().getSharedPreferences(
//                "com.example.shopsync.android.token",
//                Context.MODE_PRIVATE
//            )
//            val usersList = sharedPreferences.getString("authToken", "")
//                ?.let { APICall.getUsers(authToken = it) }
//            Log.e("USERS LIST:", "${usersList}")
//
//            _loginState.value = _loginState.value.copy(isLoading = false)
//        }
//    }


    private fun onLoginResponse(response: LoginSignupResponse, email: String) {
        viewModelScope.launch {
            _loginState.value = _loginState.value.copy(isLoading = true)
            if (response.data.token.isNotEmpty()) {
                saveTokenToSharedPreferences(response.data.token)
                saveEmailToSharedPreferences(email)
                loggedInEvent.emit(response.data.token, email)
//                loggedInEvent.emit(email)
                _navigateToHome.value = true
            } else {
                Log.e("ERROR:", "error in onLoginResponse")
            }
            _loginState.value = _loginState.value.copy(isLoading = false)
        }
    }

    private fun onSignupResponse(response: LoginSignupResponse, email: String) {
        viewModelScope.launch {
            _loginState.value = _loginState.value.copy(isLoading = true)
            if (response.data.token.isNotEmpty()) {
                saveTokenToSharedPreferences(response.data.token)
                saveEmailToSharedPreferences(email)

                loggedInEvent.emit(response.data.token, email)
                _navigateToHome.value = true
            } else {
                Log.e("ERROR:", "error in onSignupResponse")
//                _snackbarMessage.value = response.message
            }
            _loginState.value = _loginState.value.copy(isLoading = false)
        }
    }

    fun resetNavigateToHome() {
        _navigateToHome.value = false
    }
}

data class LoginState(
    val isLoading: Boolean = false,
    val isLoggedIn: Boolean = false
)

data class UsernameState(
    val isLoading: Boolean = false,
    val text: String = ""
)

data class PasswordState(
    val isLoading: Boolean = false,
    val text: String = ""
)