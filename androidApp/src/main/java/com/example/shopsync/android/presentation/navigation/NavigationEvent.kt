package com.example.shopsync.android.presentation.navigation

sealed class NavigationEvent {
    object NavigateToLoginScreen : NavigationEvent()
    data class NavigateToListScreen(val listId: Int, val listName: String) : NavigationEvent()
    // Add other navigation events if needed
}