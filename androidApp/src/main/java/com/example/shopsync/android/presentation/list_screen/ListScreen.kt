package com.example.shopsync.android.presentation.list_screen

import android.annotation.SuppressLint
import android.util.Log
import androidx.activity.OnBackPressedCallback
import androidx.activity.compose.LocalOnBackPressedDispatcherOwner
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.FabPosition
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.Modifier
import androidx.compose.material.Scaffold
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Save
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.shopsync.android.presentation.HomeScreenViewModel
import com.example.shopsync.android.presentation.list_screen.items.ShoppingListItems
import com.example.shopsync.android.presentation.list_screen.title.ShoppingListTitle

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ListScreen(
    modifier: Modifier
) {
    val listScreenViewModel: ListScreenViewModel = hiltViewModel()
    val listScreenState by listScreenViewModel.state.collectAsState()
    val shoppingListItemsState by listScreenViewModel.shoppingListItemsState.collectAsState()
    val listIdentification = listScreenState.listId
    Log.e("listIdentification:", "$listIdentification")
    listScreenViewModel.updateListId(listIdentification)

//    val listHasBeenUpdate = remember { mutableStateOf(false)}
    val updatedState = listScreenViewModel.listUpdatedState.collectAsState()
//    val listHasBeenUpdate = remember { mutableStateOf(updatedState.value) }
    val listHasBeenUpdate = updatedState.value

    listScreenViewModel.getListItems()

    val onBackPressedDispatcher = LocalOnBackPressedDispatcherOwner.current?.onBackPressedDispatcher

    val homeScreenViewModel: HomeScreenViewModel = hiltViewModel()

    Box(modifier = Modifier.fillMaxSize()) {
        Scaffold(
            modifier,
            backgroundColor = MaterialTheme.colors.surface,
            topBar = {
                ListTopBar(
                    onLogoutClick = {
                        homeScreenViewModel.logout()
                    },
                    title = "ShopSync",
                    backgroundColor = MaterialTheme.colors.secondaryVariant,
                    contentColor = MaterialTheme.colors.onBackground,
                    onArrowClick = {
                        Log.e("ARROW BEING CLICKED:", "listHasBeenUpdate: $listHasBeenUpdate")
                        listScreenViewModel.arrowClicked()
                    },
                    listScreenViewModel
                )
            },
            floatingActionButton = {
                FloatingActionButton(
                    backgroundColor = MaterialTheme.colors.secondaryVariant,
                    onClick = {
                        // todo - triggerNewItem()
                        listScreenViewModel.triggerNewItem()
                    }) {
                    Icon(Icons.Default.Add, contentDescription = "Add List Item")
                }
            },
            floatingActionButtonPosition = FabPosition.End,
            content = {
                Column {
                    if (listScreenState.isLoading) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            CircularProgressIndicator()
                        }
                    } else {
//                        listScreenViewModel.getListItems()
                        ShoppingListTitle(
                            listName = listScreenState.title,
                            listScreenViewModel,
                        )
                        Log.e("shoppingListItemsState:::", "$shoppingListItemsState")
                        if (shoppingListItemsState.isEmpty() || shoppingListItemsState.first().id == -1) {
                            // TODO - add logic for adding new item for new list

                            CircularProgressIndicator()
                        } else {
                            ShoppingListItems(
//                            listItems = listScreenState.shoppingListItems,
//                                listItems = shoppingListItemsState,
                                listScreenViewModel
                            )
                        }

                    }
                }
            }
        )

        if (listHasBeenUpdate) {
            Box(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(bottom = 16.dp)
            ) {
                FloatingActionButton(
                    modifier = Modifier.size(84.dp),
                    onClick = {
                        Log.e("FAB CLICKED:", "SAVE FABCLICKED")
                        listScreenViewModel.saveListItems()
//                        listScreenViewModel.updateListState(false)
                    },
                    backgroundColor = MaterialTheme.colors.secondaryVariant
                )
                {
                    Icon(
                        Icons.Default.Save,
                        contentDescription = "Save Shopping List",
                        modifier = Modifier.size(36.dp)
                    )
                }
            }
        }
    }

    DisposableEffect(Unit) {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                listScreenViewModel.arrowClicked()
            }
        }
        onBackPressedDispatcher?.addCallback(callback)
        onDispose {
            callback.remove()
            listScreenViewModel.updateListState(false)
        }
    }
}