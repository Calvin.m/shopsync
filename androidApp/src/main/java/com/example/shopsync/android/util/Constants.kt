package com.example.shopsync.android.util

import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CustomTextFieldColors(): TextFieldColors {
    return TextFieldDefaults.textFieldColors(
        textColor = MaterialTheme.colors.onSurface,
//        containerColor = MaterialTheme.colors.surface,
        containerColor = Color(255, 255, 255, 0),
        cursorColor = MaterialTheme.colors.secondaryVariant,
        focusedIndicatorColor = MaterialTheme.colors.primaryVariant,
        unfocusedIndicatorColor = MaterialTheme.colors.primaryVariant,
        disabledIndicatorColor = MaterialTheme.colors.primaryVariant,
        disabledTextColor = MaterialTheme.colors.onSurface,
        errorCursorColor = MaterialTheme.colors.onSurface,
        errorIndicatorColor = MaterialTheme.colors.onSurface,
        focusedLeadingIconColor = MaterialTheme.colors.onSurface,
        unfocusedLeadingIconColor = MaterialTheme.colors.onSurface,
        disabledLeadingIconColor = MaterialTheme.colors.onSurface,
        errorLeadingIconColor = MaterialTheme.colors.onSurface,
        focusedTrailingIconColor = MaterialTheme.colors.onSurface,
        unfocusedTrailingIconColor = MaterialTheme.colors.onSurface,
        disabledTrailingIconColor = MaterialTheme.colors.onSurface,
        errorTrailingIconColor = MaterialTheme.colors.onSurface,
        focusedLabelColor = MaterialTheme.colors.onSurface,
        unfocusedLabelColor = MaterialTheme.colors.onSurface,
        disabledLabelColor = MaterialTheme.colors.onSurface,
        errorLabelColor = MaterialTheme.colors.onSurface,
        placeholderColor = MaterialTheme.colors.onSurface,
        disabledPlaceholderColor = MaterialTheme.colors.onSurface,
        focusedSupportingTextColor = MaterialTheme.colors.onSurface,
        unfocusedSupportingTextColor = MaterialTheme.colors.onSurface,
        disabledSupportingTextColor = MaterialTheme.colors.onSurface,
        errorSupportingTextColor = MaterialTheme.colors.onSurface
    )
}
