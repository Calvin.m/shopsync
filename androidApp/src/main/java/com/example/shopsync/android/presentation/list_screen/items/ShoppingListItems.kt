package com.example.shopsync.android.presentation.list_screen.items

import android.util.Log
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.unit.dp
import com.example.shopsync.android.presentation.list_screen.ListScreenViewModel
import com.example.shopsync.android.util.CustomTextFieldColors
import com.example.shopsync.service.ShoppingListItem

@Composable
fun ShoppingListItems(
//    listItems: List<ShoppingListItem>,
    listScreenViewModel: ListScreenViewModel
) {
    val customTextFieldColors = CustomTextFieldColors()
//    Log.e("listIems: ", "listItems in ShoppingListItems top: $listItems")

    val state = listScreenViewModel.state.collectAsState()
    val listItems = listScreenViewModel.shoppingListItemsState.collectAsState()
    val listId = remember { mutableStateOf(state.value.listId) }
//    val uncheckedItems = remember { mutableStateListOf<Pair<String, Boolean>>() }
//    val checkedItems = remember { mutableStateListOf<Pair<String, Boolean>>() }
//    val checkedItems = remember { mutableStateListOf(ShoppingListItem()) }
//    val checkedItems = remember { mutableStateListOf<Pair<String, Boolean>>() }

    val newListItemId by listScreenViewModel.newListItemId.collectAsState()

    // TODO - might need to move these to state values in ViewModel to keep track of list items
    val uncheckedItems = remember { mutableStateListOf<ShoppingListItem>() }
    val checkedItems = remember { mutableStateListOf<ShoppingListItem>() }
    val uncheckedFocusRequesters = remember { mutableStateListOf<FocusRequester>() }
    val checkedFocusRequesters = remember { mutableStateListOf<FocusRequester>() }

    val isInitialState = remember { mutableStateOf(true) }

    val updatedListItems = remember { mutableStateListOf(ShoppingListItem) }
//    val updatedMutListItems = remember { mutableStateOf(listOf(ShoppingListItem)) } //bad

//    LaunchedEffect(key1 = Unit, block = {
//        listScreenViewModel.clearUpdatedList()
//    })

    Log.e("listItems before LaunchedEffect:", "b4 Luanched items: $listItems")
    LaunchedEffect(listItems) {
        Log.e("listItems before LaunchedEffect:", "AFTER Luanched items: $listItems")
        uncheckedItems.clear()
        checkedItems.clear()
        Log.e("uncheckedFocusRequesters", "$uncheckedFocusRequesters")
        uncheckedFocusRequesters.clear()
        checkedFocusRequesters.clear()
        Log.e("listItems!!!:", "$listItems")

        listItems.value.forEach { item ->
            val focusRequester = FocusRequester()
            Log.e("item!!!:", "$item")
            if (item.checked) {
//                checkedItems.add(Pair(item.name, true))
                checkedItems.add(
                    ShoppingListItem(
                        id = item.id,
                        name = item.name,
                        quantity = item.quantity,
                        unit = item.unit,
                        listId = item.listId,
                        checked = item.checked
                    )
                )
                checkedFocusRequesters.add(focusRequester)
            } else {
//                uncheckedItems.add(Pair(item.name, false))
                uncheckedItems.add(
                    ShoppingListItem(
                        id = item.id,
                        name = item.name,
                        quantity = item.quantity,
                        unit = item.unit,
                        listId = item.listId,
                        checked = item.checked
                    )
                )
                uncheckedFocusRequesters.add(focusRequester)
            }
        }
    }

//    val uncheckedFocusRequesters = remember { mutableStateListOf(FocusRequester()) }
//    val checkedFocusRequesters = remember { mutableStateListOf(FocusRequester()) }


    val lastFocusedIndex = remember { mutableStateOf(-1) }
    val requestFocusForNewItem = remember { mutableStateOf(false) }
    val newItemTrigger by listScreenViewModel.newItemTrigger.collectAsState()

    fun addNewItem() {
//        if (uncheckedItems.isEmpty() || uncheckedItems.last().first.isNotEmpty()) {
        if (uncheckedItems.isEmpty() || uncheckedItems.last().name.isNotEmpty()) {
//            uncheckedItems.add(Pair("", false))
            val newId = listScreenViewModel.createNewListItem(listId.value)
            Log.e("New Id being added:", "New id to uncheckedItems: $newId")
            Log.e("LIST IDDD:", "New id to uncheckedItems: $listId")
            uncheckedItems.add(
                ShoppingListItem(
                    id = newListItemId,
                    name = "",
                    checked = false,
                    listId = listId.value,
                )
            )
            println("uncheckedFocusRequesters: $uncheckedFocusRequesters")
            uncheckedFocusRequesters += FocusRequester()
            lastFocusedIndex.value = uncheckedItems.lastIndex
            requestFocusForNewItem.value = true
        } else {
            Log.e("CHECK:", "Fill out empty items before adding new ones")
        }
    }

    LaunchedEffect(newItemTrigger) {
        if (newItemTrigger) {
//            listScreenViewModel.createNewListItem(listScreenViewModel.state.value.listId)
//            listScreenViewModel.triggerNewItem() // Reset the trigger after creating the new item

            if (uncheckedItems.isEmpty() || uncheckedItems.last().name.isNotEmpty()) {
                addNewItem()
                listScreenViewModel.updateListState(true)
            } else {
                Log.e("CHECK:", "Fill out empty items before adding new ones")
            }
        }
    }

    //    fun moveItem(item: Pair<String, Boolean>, isChecked: Boolean) {
    fun moveItem(item: ShoppingListItem, isChecked: Boolean) {
        if (isChecked) {
            val index = uncheckedItems.indexOf(item)
            uncheckedItems.remove(item)
//            checkedItems.add(item.copy(second = true))
            checkedItems.add(item.copy(checked = true))
            uncheckedFocusRequesters.removeAt(index)
            checkedFocusRequesters.add(FocusRequester()) // Add this line to update the checkedFocusRequesters list
        } else {
            val index = checkedItems.indexOf(item)
            checkedItems.remove(item)
//            uncheckedItems.add(item.copy(second = false))
            uncheckedItems.add(item.copy(checked = false))
            checkedFocusRequesters.removeAt(index) // Add this line to update the checkedFocusRequesters list
            uncheckedFocusRequesters.add(FocusRequester())
        }
    }

    LazyColumn(
        modifier = Modifier.fillMaxWidth()
    ) {
        itemsIndexed(uncheckedItems) { index, item ->
            if (uncheckedFocusRequesters.toList().isEmpty()) {
                CircularProgressIndicator()
            } else {
                ListItem(
                    uncheckedItems,
                    item,
                    index,
                    uncheckedFocusRequesters,
                    customTextFieldColors,
                    onCheckedChange = { isChecked ->
//                        listScreenViewModel.updateListState(true)
                        listScreenViewModel.addItemToUpdatedList(item.copy(checked = true))
                        moveItem(item, isChecked)
                    },
                    onItemRemove = {
                        // todo - add async db logic
                        uncheckedItems.removeAt(index)
                    }
                )
            }

        }

        item {
            TextButton(
                onClick = {
//                    if (uncheckedItems.isEmpty() || uncheckedItems.last().first.isNotEmpty()) {
                    if (uncheckedItems.isEmpty() || uncheckedItems.last().name.isNotEmpty()) {
                        addNewItem()
                        listScreenViewModel.updateListState(true)
                    } else {
                        Log.e("CHECK:", "Fill out empty items before adding new ones")
                    }
                },
                modifier = Modifier.padding(top = 8.dp)
            ) {
                Text("Add New Item", color = MaterialTheme.colors.secondaryVariant)
            }
        }

        itemsIndexed(checkedItems) { index, item ->

            if (checkedFocusRequesters.toList().isEmpty()) {
                CircularProgressIndicator()
            } else {
                ListItem(
                    checkedItems,
                    item,
                    index,
                    checkedFocusRequesters,
                    customTextFieldColors,
                    onCheckedChange = { isChecked ->
//                        listScreenViewModel.updateListState(true)
//                        listScreenViewModel.addItemToUpdatedList(item)
                        listScreenViewModel.addItemToUpdatedList(item.copy(checked = false))
                        moveItem(item, isChecked)
                    },
                    onItemRemove = {
                        checkedItems.removeAt(index)
                    }
                )
            }
        }
    }

    LaunchedEffect(key1 = lastFocusedIndex.value, key2 = isInitialState.value) {
        if (!isInitialState.value && lastFocusedIndex.value >= 0 && lastFocusedIndex.value < uncheckedFocusRequesters.size) {
            uncheckedFocusRequesters[lastFocusedIndex.value].requestFocus()
        }
    }

    LaunchedEffect(requestFocusForNewItem.value, isInitialState.value) {
        if (!isInitialState.value && requestFocusForNewItem.value && uncheckedItems.isNotEmpty()) {
            uncheckedFocusRequesters.last().requestFocus()
            requestFocusForNewItem.value = false
        }
    }

    LaunchedEffect(listId) {
        if (listId.value != null && listId.value != 0) {
            listScreenViewModel.getListItems()
        }
    }
}
