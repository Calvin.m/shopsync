package com.example.shopsync.android

import android.util.Log
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

// Light Color Palette
private val LightColorPalette = lightColors(
    primary = Color(0x70, 0xAD, 0xC7),
    primaryVariant = Color(0xC1, 0xD0, 0xD6, 0xFF),
//    primaryVariant = Color(0xB5, 0xD2, 0xDF, 0xFF),
//    primaryVariant = Color(0x4A, 0x95, 0xB6),
    secondary = Color(0x4A, 0x95, 0xB6),
    secondaryVariant = Color(0x2A, 0x75, 0x9A),
    background = Color(0xF8, 0xFB, 0xFF),
    surface = Color(0xF8, 0xFB, 0xFF, 0xD9),
    onPrimary = Color(0xFF, 0xFF, 0xFF),
    onSecondary = Color(0xFF, 0xFF, 0xFF),
    onBackground = Color(0x33, 0x33, 0x33),
    onSurface = Color(0x33, 0x33, 0x33),
    error = Color(0xFF, 0x3B, 0x30),
    onError = Color(0xFF, 0xFF, 0xFF)
)

// Dark Color Palette
private val DarkColorPalette = darkColors(
    primary = Color(0x70, 0xAD, 0xC7),
    primaryVariant = Color(0xB5, 0xD2, 0xDF, 0xFF),
//    primaryVariant = Color(0x4A, 0x95, 0xB6),
    secondary = Color(0x4A, 0x95, 0xB6),
    secondaryVariant = Color(0x2A, 0x75, 0x9A),
    background = Color(0xF8, 0xFB, 0xFF),
    surface = Color(0xF8, 0xFB, 0xFF, 0xD9),
    onPrimary = Color(0xFF, 0xFF, 0xFF),
    onSecondary = Color(0xFF, 0xFF, 0xFF),
    onBackground = Color(0x33, 0x33, 0x33),
    onSurface = Color(0x33, 0x33, 0x33),
    error = Color(0xFF, 0x3B, 0x30),
    onError = Color(0xFF, 0xFF, 0xFF)
)


@Composable
fun MyApplicationTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        content = content
    )
}

val Typography by lazy {
    androidx.compose.material.Typography(
        h1 = TextStyle(
            fontFamily = RobotoFont,
            fontWeight = FontWeight.Bold,
            fontSize = 30.sp,
            lineHeight = 28.sp,
            letterSpacing = .25.sp
        ),
        h2 = TextStyle(
            fontFamily = RobotoFont,
            fontWeight = FontWeight.Bold,
            fontSize = 16.sp,
            lineHeight = 24.sp,
            letterSpacing = .15.sp
        ),
        h3 = TextStyle(
            fontFamily = RobotoFont,
            fontWeight = FontWeight.Bold,
            fontSize = 22.sp,
        ),
        body1 = TextStyle(
            fontFamily = RobotoFont,
            fontWeight = FontWeight.Normal,
            fontSize = 16.sp,
            lineHeight = 24.sp,
            letterSpacing = .15.sp
        ),
        body2 = TextStyle(
            fontFamily = RobotoFont,
            fontWeight = FontWeight.Normal,
            fontSize = 14.sp,
            lineHeight = 20.sp,
            letterSpacing = .25.sp
        ),
        caption = TextStyle(
            fontFamily = RobotoFont,
            fontWeight = FontWeight.Normal,
            fontSize = 12.sp,
            lineHeight = 16.sp,
            letterSpacing = .4.sp
        ),
        subtitle1 = TextStyle(
            fontFamily = RobotoFont,
            fontWeight = FontWeight.Medium,
            fontSize = 14.sp,
            lineHeight = 24.sp,
            letterSpacing = .15.sp
        ),
        subtitle2 = TextStyle(
            fontFamily = RobotoFont,
            fontWeight = FontWeight.Medium,
            fontSize = 14.sp,
            lineHeight = 20.sp,
            letterSpacing = .1.sp
        ),
    )
}

val RobotoFont = FontFamily(
    Font(R.font.roboto_regular, FontWeight.Normal),
    Font(R.font.roboto_bold, FontWeight.Bold),
    Font(R.font.roboto_thin, FontWeight.Thin),
    Font(R.font.roboto_light, FontWeight.Light),
    Font(R.font.roboto_black, FontWeight.Black),
)

val UbuntuFont = FontFamily(
    Font(R.font.ubuntu_regular, FontWeight.Normal),
    Font(R.font.ubuntu_bold, FontWeight.Bold),
    Font(R.font.ubuntu_light, FontWeight.Light),
    Font(R.font.ubuntu_medium, FontWeight.Medium)
//    Font(R.font.ttnorms_thin, FontWeight.Thin),
//    Font(R.font.ttnorms_black, FontWeight.Black),
)

