package com.example.shopsync.android.util

import android.content.Context

fun getTokenFromSharedPreferences(context: Context): String? {
    val sharedPreferences = context.getSharedPreferences("ShopSyncPrefs", Context.MODE_PRIVATE)
    return sharedPreferences.getString("jwtToken", null)
}

fun getEmailFromSharedPreferences(context: Context): String? {
    val sharedPreferences = context.getSharedPreferences("ShopSyncPrefs", Context.MODE_PRIVATE)
    return sharedPreferences.getString("email", null)
}

fun getIdFromSharedPreferences(context: Context): String? {
    val sharedPreferences = context.getSharedPreferences("ShopSyncPrefs", Context.MODE_PRIVATE)
    return sharedPreferences.getString("id", null)
}