package com.example.shopsync.android.presentation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition
import com.example.shopsync.android.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Composable
fun SplashAnimationComponent(
    navigateForward: () -> Unit
) {
//    val composition by rememberLottieComposition(spec = LottieCompositionSpec.RawRes(R.raw.shopping_list))
//    val composition by rememberLottieComposition(spec = LottieCompositionSpec.RawRes(R.raw.shopping_cart))
    val composition by rememberLottieComposition(
//        spec = LottieCompositionSpec.RawRes(R.raw.groceries_basket)
        spec = LottieCompositionSpec.RawRes(R.raw.shopping_cart_simple)
    )
    val progress by animateLottieCompositionAsState(composition)

    LaunchedEffect(progress) {
        if (progress == 1f) {
            withContext(Dispatchers.Main) {
                navigateForward()
            }
        }
    }
    LottieAnimation(
        composition = composition,
        progress = progress,
    )
}
