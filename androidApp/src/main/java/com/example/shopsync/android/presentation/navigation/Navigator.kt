package com.example.shopsync.android.presentation.navigation

import androidx.navigation.NavController
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Inject
import javax.inject.Singleton

interface Navigator {
    // 1
    fun setController(navController: NavController)
    // 2
    fun navigate(route: NavigationRoute)
    // 3
    fun popBackStack()
    // 4
    fun clear()
}


class NavigatorImpl @Inject constructor() : Navigator {

    // 1
    private var navController: NavController? = null

    // 2
    override fun setController(navController: NavController) {
        this.navController = navController
    }

    // 3
    override fun navigate(route: NavigationRoute) {
        navController?.navigate(
            route.buildRoute()
        )
    }

    // 4
    override fun popBackStack() {
        navController?.popBackStack()
    }

    // 5
    override fun clear() {
        navController = null
    }
}

@Module
@InstallIn(SingletonComponent::class)
interface NavigatorModule {
    @Binds
    @Singleton
    fun bindNavigator(navigatorImpl: NavigatorImpl): Navigator
}