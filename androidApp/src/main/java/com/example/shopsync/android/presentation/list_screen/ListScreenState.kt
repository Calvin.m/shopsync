package com.example.shopsync.android.presentation.list_screen

import com.example.shopsync.service.ShoppingListItem

//data class ListScreenState(
//    val isLoading: Boolean = false,
//    val title: String = "",
//    val userId: Int = 99,
//    val shoppingListItems: List<ShoppingListItem> = mutableListOf(),
//    val listId: Int = 99
//)
 data class ListScreenState(
    val listId: Int,
    val isLoading: Boolean = false,
    val title: String = "untitled",
    val userId: Int = 99,
    val shoppingListItems: List<ShoppingListItem> = mutableListOf(),
//    val listUpdated: Boolean = false
//    val returnedData:
 )