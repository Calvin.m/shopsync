package com.example.shopsync.android

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val application: Application
) : ViewModel() {

//    fun hasValidToken(): Boolean {
//        val sharedPreferences = application.getSharedPreferences(
//            "ShopSyncPrefs",
//            Context.MODE_PRIVATE
//        )
//        val token = sharedPreferences.getString("jwtToken", null)
//        return token != null // return true if the token exists
//    }
}