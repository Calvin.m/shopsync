package com.example.shopsync.android.navigation

sealed class Screens(val route: String, val hasArgument: Boolean = false) {
    object SplashScreen : Screens("splash_screen")
    object HomeScreen : Screens("home_screen")
    object LoginScreen : Screens("login_screen")
    object ListScreen : Screens("list_screen/{list_id}/{list_name}", true) {
        const val routeTemplate = "list_screen/{list_id}/{list_name}"
        fun route(listId: Int?, listName: String?) = "list_screen/${listId ?: "null"}/${listName ?: "null"}"
    }
}
