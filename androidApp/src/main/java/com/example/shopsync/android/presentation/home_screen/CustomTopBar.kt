package com.example.shopsync.android.presentation.home_screen

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
//import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun CustomTopBar(
//    token: String,
    onLogoutClick: () -> Unit,
    title: String,
    backgroundColor: Color,
    contentColor: Color
) {
    TopAppBar(
        modifier = Modifier
            .fillMaxWidth(),
        backgroundColor = MaterialTheme.colors.primary,
        content = {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(text = title, style = MaterialTheme.typography.h6)
//                Icon(Icons.Default.ArrowBack, contentDescription = "Back Home")

                Button(
                    colors = ButtonDefaults.buttonColors(backgroundColor),
                    onClick = onLogoutClick,
                    content = {
                        Text("Logout")
                    }
                )
            }
        }
    )
}