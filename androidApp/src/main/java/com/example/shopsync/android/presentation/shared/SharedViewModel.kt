package com.example.shopsync.android.presentation.shared

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.shopsync.android.di.LoggedInEvent
import com.example.shopsync.android.util.TokenManager
import com.example.shopsync.data.ApiResponse
import com.example.shopsync.data.repository.ShopSyncRepo
import com.example.shopsync.service.ShoppingList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SharedViewModel @Inject constructor(
    application: Application,
//    private val tokenManager: TokenManager,
//    private val loggedInEvent: LoggedInEvent

) : AndroidViewModel(application) {
    private val APICall = ShopSyncRepo()

    private val _shoppingLists = MutableStateFlow<List<ShoppingList>>(emptyList())
    val shoppingLists: StateFlow<List<ShoppingList>> = _shoppingLists

    private val _selectedList = MutableStateFlow(ShoppingList())
    val selectedList: StateFlow<ShoppingList> get() = _selectedList

    val sharedPreferences = application.getSharedPreferences(
        "ShopSyncPrefs",
        Context.MODE_PRIVATE
    )
    val token = sharedPreferences.getString("jwtToken", null)

    fun updateAndFetchShoppingLists(listId: Int, title: String) {
        Log.e("UPDATINGG:", "Updating from shared VM: $listId, $title")
        viewModelScope.launch {
            val isSuccess = updateShoppingList(listId, title)
            if (isSuccess) {
                fetchShoppingLists()
            }
        }

    }

    suspend fun updateShoppingList(listId: Int, title: String): Boolean {
        return withContext<Boolean>(viewModelScope.coroutineContext) {
            val token = sharedPreferences.getString("jwtToken", null)
            val userId = sharedPreferences.getInt("id", 98)

            if (token != null) {
                val response = APICall.updateShoppingList(listId, title, userId, token)
                return@withContext response
            } else {
                return@withContext false
            }
        }
    }
//        viewModelScope.launch {
////            _state.update { it.copy(isLoading = true) }
//
//            val token = sharedPreferences.getString("jwtToken", null)
//            val userId = sharedPreferences.getInt("id", 98)
//
//            if (token != null) {
//                Log.e(
//                    "updateShoppingListParams:",
//                    "Params: listId: $listId, title: $title, userId: $userId, token: $token"
//                )
//                val response = APICall.updateShoppingList(listId, title, userId, token)
//                return@launch response
//            }
////            _state.update { it.copy(isLoading = false) }
//        }
//    }

    fun fetchShoppingLists() {
        viewModelScope.launch {
            // Fetch the lists from the server and update the _shoppingLists value
//            _shoppingLists.value = fetchListsFromServer()
//            _shoppingLists.value = APICall.getShoppingLists(token)
            val result = token?.let { APICall.getShoppingLists(it) }
            when (result) {
                is ApiResponse.Success -> _shoppingLists.value = result.data
                else -> _shoppingLists.value = emptyList()
            }

        }
    }


}