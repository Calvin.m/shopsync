package com.example.shopsync.android.presentation

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.*
//import com.example.shopsync.APICall
import com.example.shopsync.android.di.LoggedInEvent
import com.example.shopsync.android.presentation.navigation.NavigationEvent
import com.example.shopsync.android.presentation.navigation.NavigationListRoute
import com.example.shopsync.android.presentation.navigation.NavigationLoginRoute
import com.example.shopsync.android.presentation.navigation.NavigationSplashRoute
import com.example.shopsync.android.presentation.navigation.Navigator
import com.example.shopsync.android.util.TokenManager
import com.example.shopsync.data.ApiResponse
import com.example.shopsync.data.UserListResponseItem
import com.example.shopsync.data.repository.ShopSyncRepo
import com.example.shopsync.service.ShoppingList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeScreenViewModel @Inject constructor(
    application: Application,
    private val tokenManager: TokenManager,
    private val loggedInEvent: LoggedInEvent,
    private val navigator: Navigator

) : AndroidViewModel(application) {

    private val _state = MutableStateFlow(HomeState())
    val state: StateFlow<HomeState> get() = _state

    //    private val _shoppingListsState = MutableStateFlow(listOf(ShoppingList()))
    private val _shoppingListsState = MutableStateFlow<List<ShoppingList>>(emptyList())
    val shoppingListsState: StateFlow<List<ShoppingList>> get() = _shoppingListsState

    private val APICall = ShopSyncRepo()

    private val _selectedList = MutableStateFlow(ShoppingList())
    val selectedList: StateFlow<ShoppingList> get() = _selectedList

    private val _navigationEvent = mutableStateOf<NavigationEvent?>(null)
    val navigationEvent: State<NavigationEvent?> = _navigationEvent

    private val sharedPreferences: SharedPreferences = application.getSharedPreferences(
        "ShopSyncPrefs",
        Context.MODE_PRIVATE
    )

    init {
        viewModelScope.launch {
            // todo - Sometimes this is not being called or collected
            loggedInEvent.event.collect { (token, email) ->
                Log.e("USER INFO:", "Token: $token and Email: $email")
                saveUserInfo(email, token)
            }
        }
    }

    fun fetchShoppingLists() {
        viewModelScope.launch {
            _state.value = _state.value.copy(isLoading = true)

            val token = sharedPreferences.getString("jwtToken", null)
            val apiResponse = token?.let { APICall.getShoppingLists(it) }

            when (apiResponse) {
                is ApiResponse.Success -> {
                    Log.e(
                        "HomeScreenViewModel:",
                        "apiResponse from APICall.getShoppingLists -> ${apiResponse.data}"
                    )
                    _shoppingListsState.update { apiResponse.data }
                    _state.update { it.copy(isLoading = false) }
                }

                is ApiResponse.Unauthorized -> {
                    Log.e("ApiResponse.Unauthorized", "ApiResponse.Unauthorized")
                    _state.value = _state.value.copy(isLoading = false)
                    // Handle unauthorized case (e.g., navigate to the login screen)
                }

                is ApiResponse.Failure -> {
                    Log.e("ApiResponse.Failure", "ApiResponse.Failure")
                    _state.value = _state.value.copy(isLoading = false)
                    // Handle failure case (e.g., show an error message)
                }

                is ApiResponse.Loading -> {
                    Log.e("ApiResponse.Loading", "ApiResponse.Loading")
                    // You can handle the loading state here if needed
                }

                else -> {
                    Log.e("ApiResponse:", "${apiResponse}")
                    Log.e("ApiResponse:", "${this}")
                    Log.e("ELSE API RESPONSE:", "CHECK THIS")
                    // todo - may need to be updated
                    navigator.navigate(NavigationSplashRoute)
                }
            }
        }
    }

    fun selectList(shoppingList: ShoppingList) {
        Log.e("SHOPPING LIST IN VM: ", "SHOPPING LIST IN VM: $shoppingList")
        viewModelScope.launch {
            _selectedList.update {
                it.copy(
                    id = shoppingList.id,
                    name = shoppingList.name,
                    userId = shoppingList.userId,
                    items = shoppingList.items
                )
            }
        }
//        navigateToListScreen(shoppingList.id, shoppingList.name)
        // TODO TODO TODO
        Log.e("shoppingList.name:", "${shoppingList.name}")
        navigator.navigate(
            NavigationListRoute(
                listId = shoppingList.id.toString(),
                title = shoppingList.name
            )
        )
    }

    private fun clearUserData() {
        // Clear stored user data like authToken, user info, etc.
    }

    private fun navigateToLoginScreen() {
        // Trigger a navigation event to the login screen
//        _navigationEvent.value = NavigationEvent.NavigateToLoginScreen
        navigator.navigate(NavigationLoginRoute)
    }

    fun createNewList() {
        val userId = sharedPreferences.getInt("id", 98)
        val authToken = sharedPreferences.getString("jwtToken", null) ?: ""

        viewModelScope.launch {
            _state.update { it.copy(isLoading = true) }
            val newlyCreatedListId = APICall.createNewList(userId, authToken)
            Log.e("NEWLY CREATED LIST:", "${newlyCreatedListId}")
            val res = async { APICall.createFirstItem(listId = newlyCreatedListId, authToken) }
            Log.e("RES RES RES: ", "$res")
//            delay(1000L)
            _state.update { it.copy(isLoading = false) }
            val response = res.await()
            Log.e("RESPONSE SENT TO LISTROUTE:", "${response.toString()}")
//            navigator.navigate(NavigationListRoute(res.toString(), "untitled"))
            navigator.navigate(NavigationListRoute(newlyCreatedListId.toString(), "untitled"))
        }
    }

    fun navigateToListScreen(id: Int, listName: String) {
        // Trigger a navigation event to the login screen
        Log.e("IDIIDIDDIIDID:", "ID: $id")
//        _navigationEvent.value = NavigationEvent.NavigateToListScreen(
//            id,
//            listName
//        )

        navigator.navigate(NavigationListRoute(id.toString(), listName))
    }


    private fun saveUserInfo(email: String, authToken: String) {
        viewModelScope.launch {
            _state.value = _state.value.copy(isLoading = true)
            val sharedPreferences = getApplication<Application>().getSharedPreferences(
                "ShopSyncPrefs",
                Context.MODE_PRIVATE
            )
            val usersList = APICall.getUsers(authToken)
            val user: UserListResponseItem = usersList.filter { it.email == email }[0]

            sharedPreferences.edit().putString("email", user.email).apply()
            sharedPreferences.edit().putInt("id", user.id).apply()

            _state.value = _state.value.copy(isLoading = false)
        }
    }
    /*

        fun getShoppingLists(authToken: String?) {
            viewModelScope.launch {
                _state.value = _state.value.copy(isLoading = true)

                val token = sharedPreferences.getString("jwtToken", null)
                val apiResponse = token?.let { APICall.getShoppingLists(it) }

                when (apiResponse) {
                    is ApiResponse.Success -> {
                        _state.value =
                            _state.value.copy(isLoading = false, shoppingLists = apiResponse.data)
                    }

                    is ApiResponse.Unauthorized -> {
                        _state.value = _state.value.copy(isLoading = false)
                        // Handle unauthorized case (e.g., navigate to the login screen)
                    }

                    is ApiResponse.Failure -> {
                        _state.value = _state.value.copy(isLoading = false)
                        // Handle failure case (e.g., show an error message)
                    }

                    is ApiResponse.Loading -> {
                        // You can handle the loading state here if needed
                    }

                    else -> {
                        Log.e("CHECK THIS:", "CHECCKK THISS")
                    }
                }
            }
        }
    */

    private fun removeTokenFromSharedPreferences() {
        val sharedPreferences = getApplication<Application>().getSharedPreferences(
            "ShopSyncPrefs",
            Context.MODE_PRIVATE
        )
        with(sharedPreferences.edit()) {
            remove("jwtToken")
            apply()
        }
    }

    fun logout() {
        viewModelScope.launch {
            val apiResponse = APICall.logout()
            when (apiResponse) {
                is ApiResponse.Success -> {
                    Log.e("SUCCESS:", "SUCCESS")
                    removeTokenFromSharedPreferences()
                    navigateToLoginScreen()
                }

                is ApiResponse.Failure -> {
                    Log.e("FAILURE:", "${apiResponse.message}")
                }

                ApiResponse.Loading -> {
                    Log.e("Loading...:", "Loadingg......")
                }

                ApiResponse.Unauthorized -> {
                    Log.e("Unauthorized!:", "${apiResponse.toString()}")
                }

                else -> {}
            }
        }
    }
}

data class HomeState(
    val isLoading: Boolean = false,
    val shoppingLists: List<ShoppingList> = emptyList(),
)
