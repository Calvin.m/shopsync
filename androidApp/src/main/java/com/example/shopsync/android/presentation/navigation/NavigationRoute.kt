package com.example.shopsync.android.presentation.navigation

import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavType
import androidx.navigation.navArgument

interface NavigationRoute {
    fun buildRoute(): String
}

object NavigationSplashRoute : NavigationRoute {
    override fun buildRoute(): String = route

    private const val root = "splash"
    const val route = root
}

object NavigationHomeRoute : NavigationRoute {
    override fun buildRoute(): String = route

    private const val root = "home"
    const val route = root
}

object NavigationLoginRoute : NavigationRoute {
    override fun buildRoute(): String = route

    private const val root = "login"
    const val route = root
}

data class NavigationListRoute(
    val listId: String,
    val title: String
) : NavigationRoute {
    constructor(savedStateHandle: SavedStateHandle) : this(
        listId = requireNotNull(savedStateHandle.get<String>(listIdArg)),
        title = requireNotNull(savedStateHandle.get<String>(titleArg))
    )

    override fun buildRoute(): String = "$root/$listId/$title"

    companion object {
        private const val root = "list"
        private const val listIdArg = "listId"
        private const val titleArg = "title"
        const val route = "$root/{$listIdArg}/{$titleArg}"

        val navArgs = listOf(
            navArgument(name = listIdArg) {
                type = NavType.StringType
            },
            navArgument(name = titleArg) {
                type = NavType.StringType
            }
        )
    }
}