package com.example.shopsync.android.presentation.home_screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.shopsync.android.presentation.HomeScreenViewModel
import com.example.shopsync.android.presentation.HomeState
import com.example.shopsync.service.ShoppingList


@Composable
fun HomeShoppingLists(
    state: State<HomeState>,
    shoppingLists: List<ShoppingList>,
    homeScreenViewModel: HomeScreenViewModel
) {
    val filteredLists = shoppingLists // Add logic to filter the lists based on the search text

    if (state.value.isLoading) {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CircularProgressIndicator()
        }
    } else {
        if (filteredLists.isEmpty()) {
            Box(
                modifier = Modifier
                    .fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text("No shopping lists found")
            }
        } else {
            LazyColumn(
                modifier = Modifier
                    .padding(8.dp)
            ) {
                items(filteredLists) { shoppingList ->
                    HomeShoppingListCard(
                        shoppingList = shoppingList,
                        homeScreenViewModel
                    )
                }
            }
        }
    }

}