package com.example.shopsync.android.presentation.list_screen

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.shopsync.android.presentation.navigation.NavigationEvent
import com.example.shopsync.android.presentation.navigation.NavigationHomeRoute
import com.example.shopsync.android.presentation.navigation.NavigationListRoute
import com.example.shopsync.android.presentation.navigation.Navigator
import com.example.shopsync.data.repository.ShopSyncRepo
import com.example.shopsync.service.ShoppingListItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListScreenViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val navigator: Navigator,
    private val application: Application,
) : AndroidViewModel(
    application
) {
    private val APICall = ShopSyncRepo()

    private val route = NavigationListRoute(savedStateHandle)

    private val _state = MutableStateFlow(
        ListScreenState(
            listId = route.listId.toInt(),
            title = route.title
//            returnedData = null,
        )
    )
    val state: StateFlow<ListScreenState> = _state.asStateFlow()

    private val _listUpdatedState = MutableStateFlow(false)
    val listUpdatedState: StateFlow<Boolean> get() = _listUpdatedState

    private val _shoppingListItemsState = MutableStateFlow(listOf(ShoppingListItem()))
    val shoppingListItemsState: StateFlow<List<ShoppingListItem>> get() = _shoppingListItemsState

    private val _shoppingListItemsToUpdate = MutableStateFlow(listOf(ShoppingListItem()))
    val shoppingListItemsToUpdate: StateFlow<List<ShoppingListItem>> get() = _shoppingListItemsToUpdate

    private val _newListItemId = MutableStateFlow(-1)
    val newListItemId: StateFlow<Int> get() = _newListItemId

    private val _listItem = MutableStateFlow(ShoppingListItem())
    val listItem: StateFlow<ShoppingListItem> get() = _listItem

    val newItemTrigger = MutableStateFlow(false)

    fun triggerNewItem() {
        newItemTrigger.value = !newItemTrigger.value
    }

    fun updateListState(bool: Boolean) {
        viewModelScope.launch {
            _listUpdatedState.update { bool }
        }
    }

    fun clearUpdatedList() {
        viewModelScope.launch {
            _shoppingListItemsToUpdate.update { emptyList() }
        }
    }

    private val sharedPreferences: SharedPreferences = application.getSharedPreferences(
        "ShopSyncPrefs",
        Context.MODE_PRIVATE
    )


    fun createNewListItem(listId: Int) {
        val userId = sharedPreferences.getInt("id", 98)
        val authToken = sharedPreferences.getString("jwtToken", null) ?: ""

        viewModelScope.launch {
            _state.update { it.copy(isLoading = true) }
            val res = async { APICall.createFirstItem(listId = listId, authToken) }
            val response = res.await()
            _newListItemId.emit(response)
            Log.e("RESPONSE:", "${response.toString()}")
//            getListItems()
            addItemToUpdatedList(
                ShoppingListItem(
                    id = response,
                    listId = listId,
                    checked = false
                )
            )
            _state.update { it.copy(isLoading = false) }
        }

        getListItems()
//        return _newListItemId.value
    }

    fun addItemToUpdatedList(item: ShoppingListItem) {
        viewModelScope.launch {
            Log.e("current list: ", "${_shoppingListItemsToUpdate.value}")
            Log.e("ITEM BEING SAVED:", "$item")
            var bool = false
            val isInList =
                _shoppingListItemsToUpdate.value.map { if (it.id == item.id) bool = true }
            Log.e("isInList:", "$isInList")
            if (bool == false) {
                Log.e("BOOL IS FALSE:", "BOOL IS FALSE")
                _shoppingListItemsToUpdate.value = _shoppingListItemsToUpdate.value + item
                Log.e("UPDATED list!: ", "${_shoppingListItemsToUpdate.value}")
            } else if (bool == true) {
                Log.e("BOOL IS TRUE:", "BOOL IS TRUE")
                _shoppingListItemsToUpdate.value =
                    _shoppingListItemsToUpdate.value.map { if (it.id == item.id) item else it }
                Log.e("UPDATED list!: ", "${_shoppingListItemsToUpdate.value}")
            }
            _listUpdatedState.update { true }
        }
    }

    val hasUserLeftScreen = MutableStateFlow(false)

    init {
        _state.update { it.copy(isLoading = true) }
//        getListItems(state.value.listId)
        clearUpdatedList()
        _state.value =
            _state.value.copy(isLoading = false)
    }

    fun userLeftScreen() {
        hasUserLeftScreen.value = true
    }

    // TODO
    fun saveListItems() {
        viewModelScope.launch {
            // todo ---
            val authToken = sharedPreferences.getString("jwtToken", null) ?: ""
            Log.e("CALLING FOR EACH:", "CALLING FOR EACH")
            Log.e("_shoppingListItemsToUpdate.value:", "${_shoppingListItemsToUpdate.value}")

            _shoppingListItemsToUpdate.value.forEach { item ->
                APICall.saveShoppingListItem(
                    ShoppingListItem(
                        id = item.id,
                        name = item.name,
                        quantity = item.quantity,
                        unit = item.unit,
                        listId = item.listId,
                        checked = item.checked
                    ),
                    authToken
                )
            }
            _listUpdatedState.update { false }
        }
        Log.e("Clicked:", "${shoppingListItemsState.value}")
    }

    fun updateTitle(title: String) {
        Log.e("TITLE in UPDATE:", "$title")
        viewModelScope.launch {
            _state.update { it.copy(title = title) }
        }
    }

    fun getListItems() {
        _state.update { it.copy(isLoading = true) }
        val listId = state.value.listId
        Log.e("List ID:", "listId being passed to API: $listId")
        if (listId != null) {
            val sharedPreferences = application.getSharedPreferences(
                "ShopSyncPrefs",
                Context.MODE_PRIVATE
            )
            val token = sharedPreferences.getString("jwtToken", null)
            viewModelScope.launch {
                if (token != null) {
                    val itemsList = APICall.getShoppingListItems(listId, token)
                    Log.e("ListScreenViewModel:", "getListItems(): $itemsList")
//                    _state.update { it.copy(shoppingListItems = itemsList) }
                    _shoppingListItemsState.update { itemsList }
                }
            }
        }
        _state.value = _state.value.copy(isLoading = false)
    }

    fun updateListId(listId: Int?) {
        _state.update { it.copy(isLoading = true) }
        if (listId != null) {
            _state.value = _state.value.copy(listId = listId)
        }
        _state.value = _state.value.copy(isLoading = false)
    }

    fun arrowClicked() {
        viewModelScope.launch {

            Log.e("listId and title being saved:", "${state.value.listId}, ${state.value.title}")
            val title = if (state.value.title == "") "untitled" else state.value.title
            if (state.value.listId == -1) saveShoppingList(title)
            else {
//            saveShoppingList(title)
                updateShoppingList(state.value.listId, title)
            }

        }
        navigator.popBackStack()
    }

    fun navigateToHome() {
        navigator.navigate(NavigationHomeRoute)
    }

    private fun saveShoppingList(name: String = state.value.title) {
        viewModelScope.launch {
            _state.value = _state.value.copy(isLoading = true)
            val sharedPreferences = application.getSharedPreferences(
                "ShopSyncPrefs",
                Context.MODE_PRIVATE
            )
            val token = sharedPreferences.getString("jwtToken", null)
            val userId = sharedPreferences.getInt("id", 98)
            if (token != null) {
                APICall.saveShoppingList(name, userId, token)
            }
            _state.value = _state.value.copy(isLoading = false)
        }
    }

    private fun updateShoppingList(listId: Int, name: String = state.value.title) {
        viewModelScope.launch {
//            _state.update { it.copy(isLoading = true) }
            val sharedPreferences = application.getSharedPreferences(
                "ShopSyncPrefs",
                Context.MODE_PRIVATE
            )
            val token = sharedPreferences.getString("jwtToken", null)
            val userId = sharedPreferences.getInt("id", 98)

            if (token != null) {
                Log.e(
                    "updateShoppingListParams:",
                    "Params: listId: $listId, name: $name, userId: $userId, token: $token"
                )
                APICall.updateShoppingList(listId, name, userId, token)
            }
//            _state.update { it.copy(isLoading = false) }
        }
    }
}
