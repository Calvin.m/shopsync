package com.example.shopsync.android.presentation.login_screen

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.shopsync.android.R
import com.example.shopsync.android.navigation.Screens

@Composable
fun LoginScreen(
    modifier: Modifier
//    navController: NavHostController,
//    loginViewModel: LoginViewModel = hiltViewModel()
) {
    val loginViewModel: LoginViewModel = hiltViewModel()

//    val navigateToHome by loginViewModel.navigateToHome.collectAsState()
//
//    if (navigateToHome) {
//        navController.navigate(Screens.HomeScreen.route) {
//            popUpTo(Screens.LoginScreen.route) { inclusive = true }
//        }
//        loginViewModel.resetNavigateToHome() // Reset the navigation state
//    }

    val eState = loginViewModel.emailState.collectAsState()
    val pWord = loginViewModel.passwordState.collectAsState()

    fun onSignUp(email: String, password: String) {
        loginViewModel.signUp(email, password)
    }

    fun onLogin(email: String, password: String) {
        loginViewModel.login(email, password)
    }

    BoxWithConstraints(
        modifier = Modifier.fillMaxSize()
    ) {
        val width = maxWidth
        val height = maxHeight
        val painter = painterResource(id = R.drawable.supermarket_small)
        Image(
            painter = painter,
            contentDescription = "Background image",
            contentScale = ContentScale.Crop,
            modifier = Modifier.fillMaxSize()
        )
        Column(
            Modifier.padding(42.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = "Shop Sync",
                fontSize = 42.sp,
                style = MaterialTheme.typography.h1
            )
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = 8.dp)
            ) {
                Box(
                    modifier = Modifier
                        .clip(RoundedCornerShape(6.dp))
                        .fillMaxWidth(0.95f)
                        .aspectRatio(1f)
                        .background(MaterialTheme.colors.surface)
                        .align(Alignment.Center)
                ) {
                    Column(
                        Modifier
                            .padding(8.dp)
                            .fillMaxSize(),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        val emailState = remember { mutableStateOf("") }
                        val passwordState = remember { mutableStateOf("") }
                        Text(
                            text = "Login",
                            color = MaterialTheme.colors.onSurface,
                            style = MaterialTheme.typography.h3
                        )
                        Spacer(modifier = Modifier.size(16.dp))
                        OutlinedTextField(
                            value = emailState.value,
                            onValueChange = { newValue ->
                                Log.e("newValue: ", newValue)
                                emailState.value = newValue
                                Log.e("emailState.value", "${emailState.value}")
                            },
                            label = { Text(text = "Email") },
                            modifier = Modifier.fillMaxWidth(),
                            colors = TextFieldDefaults.outlinedTextFieldColors(MaterialTheme.colors.onSurface)
                        )
                        Spacer(modifier = Modifier.size(16.dp))
                        OutlinedTextField(
                            value = passwordState.value,
                            onValueChange = { newValue ->
                                passwordState.value = newValue
                                Log.e("passwordState.value", "${passwordState.value}")

                            },
                            label = { Text(text = "Password") },
                            modifier = Modifier.fillMaxWidth(),
                            colors = TextFieldDefaults.outlinedTextFieldColors(MaterialTheme.colors.onSurface)
                        )
                        Spacer(modifier = Modifier.size(16.dp))
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(start = 20.dp, top = 12.dp, end = 20.dp),
                            horizontalArrangement = Arrangement.SpaceBetween
                        ) {
                            Button(
                                elevation = ButtonDefaults.elevation(8.dp),
                                onClick = { onSignUp(emailState.value, passwordState.value) },
                                colors = ButtonDefaults.buttonColors(
                                    backgroundColor = Color(185, 185, 185, 255)
                                )
                            ) {
                                Text(
                                    text = "Signup",
                                    color = Color(53, 52, 52, 255),
                                    fontFamily = FontFamily.SansSerif
                                )
                            }
                            Button(
                                onClick = { onLogin(emailState.value, passwordState.value) },
                                modifier = Modifier.padding(start = 16.dp),
                                elevation = ButtonDefaults.elevation(8.dp),
                                colors = ButtonDefaults.buttonColors(MaterialTheme.colors.secondary)
                            ) {
                                Text(
                                    text = "Login",
                                    fontFamily = FontFamily.SansSerif
                                )
                            }
                        }
                    }
                }
            }
        }
    }

}