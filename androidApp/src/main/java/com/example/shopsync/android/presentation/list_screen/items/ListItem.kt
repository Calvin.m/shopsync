package com.example.shopsync.android.presentation.list_screen.items

import android.util.Log
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.shopsync.android.presentation.list_screen.ListScreenViewModel
import com.example.shopsync.service.ShoppingListItem

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ListItem(
    shoppingListItems: SnapshotStateList<ShoppingListItem>,
    item: ShoppingListItem,
    index: Int,
    focusRequesters: SnapshotStateList<FocusRequester>,
    customTextFieldColors: TextFieldColors,
    onCheckedChange: (Boolean) -> Unit,
    onItemRemove: () -> Unit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        val listScreenViewModel: ListScreenViewModel = hiltViewModel()
        val shoppingListItemsToUpdate =
            listScreenViewModel.shoppingListItemsToUpdate.collectAsState().value
        val newListItemId by listScreenViewModel.newListItemId.collectAsState()

        Checkbox(
            checked = item.checked,
            onCheckedChange = onCheckedChange
        )

//        Log.e("focusRequesters LI:", "${focusRequesters.toList()}")
//        Log.e("index:", "$index")
//
//        Log.e("shoppingListItems LI:", "${shoppingListItems.toList()}")
//        Log.e("index:", "$index")
        TextField(
            textStyle = MaterialTheme.typography.body1.copy(
                textDecoration = if (item.checked) TextDecoration.LineThrough else TextDecoration.None
            ),
            modifier = Modifier
                .focusRequester(focusRequesters[index]),
//            value = item.name,
            value = if (item.name == "untitled") "" else item.name,
            onValueChange = { newValue ->
                Log.e("ITEM BEING CHANED:", "$item")
                Log.e("newListItemId:", "$newListItemId")
                shoppingListItems[index] = ShoppingListItem(
                    id = if (item.id == -1) newListItemId else item.id,
                    name = newValue,
                    checked = item.checked,
                    listId = item.listId,
                    quantity = item.quantity,
                    unit = item.unit
                )
//                val pos = shoppingListItemsToUpdate.indexOf(item.first)
                listScreenViewModel.addItemToUpdatedList(
                    item.copy(
                        id = if (item.id == -1) newListItemId else item.id,
                        name = newValue,
                        checked = item.checked,
                        listId = item.listId,
                        quantity = item.quantity,
                        unit = item.unit
                    )
                )
//                if (shoppingListItemsToUpdate.contains(item))
                listScreenViewModel.updateListState(true)
            },
            placeholder = { Text("Item Name", color = Color(122, 145, 151, 255)) },
            colors = customTextFieldColors,
            shape = RoundedCornerShape(0.dp) // To remove the outline and make it appear as a filled TextField
        )

        IconButton(onClick = onItemRemove, Modifier.weight(1f)) {
            Icon(
                imageVector = Icons.Default.Close,
                contentDescription = null,
                tint = MaterialTheme.colors.primary
            )
        }
    }
}