package com.example.shopsync.android.presentation.list_screen

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
//import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.shopsync.android.navigation.Screens

@Composable
fun ListTopBar(
//    token: String,
    onLogoutClick: () -> Unit,
    title: String,
    backgroundColor: Color,
    contentColor: Color,
    onArrowClick: () -> Unit,
    listScreenViewModel: ListScreenViewModel,
//    navController: NavController
) {
//    val viewModel = ListScreenViewModel()
//    val listViewModel
    TopAppBar(
        modifier = Modifier.fillMaxWidth(),
        backgroundColor = MaterialTheme.colors.primary,
        content = {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
//                Text(text = title, style = MaterialTheme.typography.h6)
//                androidx.compose.material3.IconButton(onClick = {
//                    listScreenViewModel.userLeftScreen()
////                    navController.navigateUp()
//                }) {
//                    Icon(Icons.Default.ArrowBack, contentDescription = "Back Home")
//                }
                androidx.compose.material3.IconButton(onClick = onArrowClick) {
                    Icon(Icons.Filled.ArrowBack, contentDescription = "Back")
                }


                Button(
                    colors = ButtonDefaults.buttonColors(backgroundColor),
                    onClick = onLogoutClick,
                    content = {
                        Text("Logout")
                    }
                )
            }
        }
    )
}