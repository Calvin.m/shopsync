package com.example.shopsync.android

import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.OnBackPressedCallback
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
//import com.example.shopsync.android.navigation.SetUpNavGraph
import com.example.shopsync.android.navigation.Screens
import com.example.shopsync.android.presentation.HomeScreen
import com.example.shopsync.android.presentation.HomeScreenViewModel
import com.example.shopsync.android.presentation.SplashScreen
import com.example.shopsync.android.presentation.list_screen.ListScreen
import com.example.shopsync.android.presentation.list_screen.ListScreenViewModel
import com.example.shopsync.android.presentation.login_screen.LoginScreen
import com.example.shopsync.android.presentation.navigation.NavigationHomeRoute
import com.example.shopsync.android.presentation.navigation.NavigationListRoute
import com.example.shopsync.android.presentation.navigation.NavigationLoginRoute
//import com.example.shopsync.android.presentation.login_screen.LoginViewModel
import com.example.shopsync.android.presentation.navigation.NavigationSplashRoute
import com.example.shopsync.android.presentation.shared.SharedViewModel
import com.example.shopsync.android.presentation.navigation.Navigator
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

//    @Inject
//    lateinit var loginViewModel: LoginViewModel

    @Inject
    lateinit var navigator: Navigator

    fun getValidToken(): String? {
        val sharedPreferences = getSharedPreferences(
            "ShopSyncPrefs",
            Context.MODE_PRIVATE
        )
        return sharedPreferences.getString("jwtToken", null)
    }

    private val mainViewModel: MainViewModel by viewModels()
    private val homeScreenViewModel: HomeScreenViewModel by viewModels()
    private val listScreenViewModel: ListScreenViewModel by viewModels()
    private val sharedViewModel: SharedViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {

                val navController = rememberNavController()
                DisposableEffect(key1 = navController) {
                    navigator.setController(navController)
                    onDispose {
                        navigator.clear()
                    }
                }

                NavHost(
                    navController = navController,
                    startDestination = NavigationSplashRoute.route
                ) {
                    composable(route = NavigationSplashRoute.route) {
                        SplashScreen(
                            modifier = Modifier
                        )
                    }
                    composable(route = NavigationLoginRoute.route) {
                        LoginScreen(modifier = Modifier)
                    }
                    composable(route = NavigationHomeRoute.route) {
                        HomeScreen(modifier = Modifier)
                    }
                    composable(
                        route = NavigationListRoute.route,
                        arguments = NavigationListRoute.navArgs
                    ) {
                        ListScreen(
                            modifier = Modifier
                        )
                    }
                }
            }
        }
    }
}
