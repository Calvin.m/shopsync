package com.example.shopsync.android.presentation

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavController
import com.example.shopsync.android.navigation.Screens
import com.example.shopsync.android.presentation.login_screen.LoginViewModel
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.delay

@Composable
fun SplashScreen(
    modifier: Modifier
//    navController: NavController,
//    loginViewModel: LoginViewModel,
//    hasValidToken: Boolean
) {
//    val isLoggedIn by loginViewModel.loginState.observeAsState(initial = null)
    val loginViewModel: LoginViewModel = hiltViewModel()
    val loginState by loginViewModel.loginState.collectAsState()
    val hasValidToken = loginViewModel.hasValidToken()

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(112, 173, 199)),
        contentAlignment = Alignment.Center,
    ) {
        SplashAnimationComponent {
//            when (loginState.isLoggedIn) {
            when (hasValidToken) {
                true -> {
//                    navController.navigate(Screens.HomeScreen.route) {
//                        popUpTo(Screens.SplashScreen.route) { inclusive = true }
//                    }
                    loginViewModel.navigateToHome()
                }
                false -> {
//                    navController.navigate(Screens.LoginScreen.route) {
//                        popUpTo(Screens.SplashScreen.route) { inclusive = true }
//                    }
                    loginViewModel.navigateToLogin()
                }
                else -> {
                    // Wait for login state to be determined (e.g., check SharedPreferences)
                    println("ELSE in SplashScreen")
                }
            }

//            navController.navigate(Screens.LoginScreen.route) {
//                popUpTo(Screens.SplashScreen.route) { inclusive = true }
//            }
        }
    }


    LaunchedEffect(Unit) {
        // Assuming you have SPLASH_SCREEN_DURATION defined, otherwise use a specific value
        delay(1000L)

//        val hasValidToken = loginViewModel.hasValidToken()
//        if (hasValidToken) {
//            navController.navigate(Screens.HomeScreen.route) {
//                popUpTo(Screens.SplashScreen.route) { inclusive = true }
//            }
//        } else {
//            navController.navigate(Screens.LoginScreen.route) {
//                popUpTo(Screens.SplashScreen.route) { inclusive = true }
//            }
//        }
        when (hasValidToken) {
            true -> {
//                    navController.navigate(Screens.HomeScreen.route) {
//                        popUpTo(Screens.SplashScreen.route) { inclusive = true }
//                    }
                loginViewModel.navigateToHome()
            }
            false -> {
//                    navController.navigate(Screens.LoginScreen.route) {
//                        popUpTo(Screens.SplashScreen.route) { inclusive = true }
//                    }
                loginViewModel.navigateToLogin()
            }
            else -> {
                // Wait for login state to be determined (e.g., check SharedPreferences)
                println("ELSE in SplashScreen")
            }
        }
    }
}