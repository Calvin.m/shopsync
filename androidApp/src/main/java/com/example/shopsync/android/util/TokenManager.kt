package com.example.shopsync.android.util

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TokenManager @Inject constructor(@ApplicationContext context: Context) {

    private val sharedPreferences: SharedPreferences = context.getSharedPreferences(
        "ShopSyncPrefs",
        Context.MODE_PRIVATE
    )

    fun getToken(): String {
        return sharedPreferences.getString("jwtToken", "") ?: ""
    }

    // ...
}
