package com.example.shopsync.android.presentation.list_screen.title

import android.util.Log
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.shopsync.android.presentation.list_screen.ListScreenViewModel

@Composable
fun ShoppingListTitle(
    listName: String?,
    listScreenViewModel: ListScreenViewModel,
) {
    val focusRequester = remember { FocusRequester() }
    var isFocused by remember { mutableStateOf(false) }
    var isNewList by remember { mutableStateOf(false) }

//    if (listScreenViewModel.state.collectAsState().value.title.isEmpty()) {
    if (listName == "untitled") {
        isNewList = true
        listScreenViewModel.updateTitle("")
    } else if (listName != null) {
        listScreenViewModel.updateTitle(listName)
    }
//    }

    Log.e("NEW LIST??;", "$isNewList")

    val titleState = listScreenViewModel.state.collectAsState()

    Row(
        Modifier
            .fillMaxWidth()
            .padding(16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        BasicTextField(
            value = titleState.value.title,
            onValueChange = { txt ->
                listScreenViewModel.updateTitle(txt)
            },
            modifier = Modifier
                .focusRequester(focusRequester)
                .onFocusChanged { focusState ->
                    isFocused = focusState.isFocused
                },
            textStyle = TextStyle(
                color = if (titleState.value.title.isNotEmpty() || isFocused) Color.Black else Color.Gray,
                fontSize = 24.sp
            ),
            decorationBox = { innerTextField ->
                if (titleState.value.title.isEmpty() && !isFocused) {
                    Text("untitled", style = TextStyle(color = Color.Gray, fontSize = 24.sp))
                }
                innerTextField()
            },
            singleLine = true,
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(onDone = { focusRequester.freeFocus() })
        )
        Spacer(modifier = Modifier.weight(1f))
        androidx.compose.material3.IconButton(onClick = {
            // todo -> share, delete, check all, uncheck all menu list of options
        }) {
            androidx.compose.material3.Icon(
                imageVector = Icons.Default.MoreVert,
                contentDescription = null,
                tint = MaterialTheme.colorScheme.onSurface
            )
        }
    }
}
