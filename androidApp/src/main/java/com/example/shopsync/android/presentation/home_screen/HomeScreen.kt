package com.example.shopsync.android.presentation

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.shopsync.android.presentation.home_screen.CustomTopBar
import com.example.shopsync.android.presentation.home_screen.HomeShoppingLists
//import com.example.shopsync.android.presentation.home_screen.CustomScaffold
//import com.example.shopsync.android.presentation.home_screen.CustomTopAppBar

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun HomeScreen(
    modifier: Modifier
) {
    val homeScreenViewModel: HomeScreenViewModel = hiltViewModel()
    val state = homeScreenViewModel.state.collectAsState()
    val shoppingLists = homeScreenViewModel.shoppingListsState.collectAsState()

    Log.e("HomeScreen.kt", "STATE: ${state.value}")

    LaunchedEffect(Unit) {
        homeScreenViewModel.fetchShoppingLists()
    }

    Scaffold(
        modifier = Modifier,
        backgroundColor = MaterialTheme.colors.primary,
//        bottomBar = {
//            MyAppBottomBar(navController)
//        },
        topBar = {
            CustomTopBar(
                onLogoutClick = { homeScreenViewModel.logout() },
                title = "ShopSync",
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                contentColor = MaterialTheme.colors.onBackground
            )
        },
        floatingActionButton = {
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                onClick = {
                    homeScreenViewModel.createNewList()
//                    homeScreenViewModel.navigateToListScreen(-1, "untitled")
                }) {
                Icon(Icons.Default.Add, contentDescription = "Add Shopping List")
            }
        },
        floatingActionButtonPosition = FabPosition.End,
        content = {
            if (state.value.isLoading) {
                CircularProgressIndicator()
            } else {
                Column {
                    SearchBar()
                    HomeShoppingLists(
                        state,
                        shoppingLists.value,
                        homeScreenViewModel
                    )
                }
            }
        }
    )
}

@Composable
fun SearchBar() {
    var searchText by remember { mutableStateOf("") }

    TextField(
        colors = TextFieldDefaults.textFieldColors(MaterialTheme.colors.onBackground),
        value = searchText,
        onValueChange = { searchText = it },
        label = { Text("Search", color = MaterialTheme.colors.onBackground) },
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        singleLine = true,
        leadingIcon = {
            Icon(
                Icons.Default.Search,
                contentDescription = "Search Icon",
                tint = MaterialTheme.colors.onBackground
            )
        }
    )
}
