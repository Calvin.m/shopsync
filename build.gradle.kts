//plugins {
//    //trick: for the same plugin versions in all sub-modules
//    id("com.android.application").version("7.4.1").apply(false)
//    id("com.android.library").version("7.4.1").apply(false)
////    kotlin("android").version("1.7.0").apply(false)
//    kotlin("jvm").version("1.7.10").apply(false)
//    kotlin("multiplatform").version("1.7.0").apply(false)
////    id("serialization").version("1.5.0").apply(false)
//}
buildscript {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.7.10")
        classpath("com.android.tools.build:gradle:8.0.1")
        classpath("com.squareup.sqldelight:gradle-plugin:1.5.3")
        classpath("com.google.dagger:hilt-android-gradle-plugin:2.45")
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
    }

    configurations.all {
        resolutionStrategy {
            force("org.jetbrains.kotlin:kotlin-stdlib:1.8.10")
            force("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.8.10")
            force("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.8.10")
        }
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
