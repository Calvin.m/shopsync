//package com.example.shopsync
//
//import com.example.shopsync.data.ShopSyncApi
//import com.example.shopsync.di.createHttpClient
//import kotlinx.coroutines.runBlocking
//import kotlin.test.Test
//
//@Test
//fun testGetShoppingLists() {
//    val httpClient = createHttpClient()
//    val api = ShopSyncApi(httpClient)
//
//    runBlocking {
//        try {
//            val authToken = "your_test_auth_token_here"
//            val shoppingLists = api.getShoppingLists(authToken)
//            println("Fetched shopping lists: $shoppingLists")
//        } catch (e: Exception) {
//            println("Error: ${e.message}")
//        }
//    }
//}