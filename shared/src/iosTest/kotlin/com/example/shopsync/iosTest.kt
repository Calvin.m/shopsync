package com.example.shopsync

import kotlin.test.Test
import kotlin.test.assertTrue

class IosGreetingTest {

    @Test
    fun testExample() {
        assertTrue(APICall().greet().contains("iOS"), "Check iOS is mentioned")
    }
}