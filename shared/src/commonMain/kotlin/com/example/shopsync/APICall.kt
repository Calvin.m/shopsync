//package com.example.shopsync
//
//import com.example.shopsync.data.ApiResponse
//import com.example.shopsync.data.ShopSyncApi
//import com.example.shopsync.di.createHttpClient
//import com.example.shopsync.service.LoginSignupResponse
//import com.example.shopsync.service.ShoppingList
//import com.example.shopsync.service.SignupResponse
//import io.ktor.client.*
//
//class APICall() {
//
//    private val client = HttpClient()
//
//    val httpClient = createHttpClient()
//    val api = ShopSyncApi(httpClient)
//
//    companion object {
//        val httpClient = createHttpClient()
//        val api = ShopSyncApi(httpClient)
//
//        suspend fun getShoppingLists(authToken: String): ApiResponse<List<ShoppingList>> {
//            val shoppingLists = api.getShoppingLists(authToken)
//            return shoppingLists
//        }
//
//        suspend fun login(email: String, password: String): ApiResponse<LoginSignupResponse> {
//            val response = api.login(email, password)
//            return response
//        }
//
//        suspend fun signup(email: String, password: String): ShopSyncApi.Response<SignupResponse> {
//            val response = api.signup(email, password)
//            return response
//        }
//    }
//}