package com.example.shopsync.data

sealed class ApiResponse<out T> {
    object Loading : ApiResponse<Nothing>()
    data class Success<out T>(val data: T) : ApiResponse<T>()
    data class Failure(val message: String, val cause: Throwable? = null) : ApiResponse<Nothing>()
    object Unauthorized : ApiResponse<Nothing>()
}
