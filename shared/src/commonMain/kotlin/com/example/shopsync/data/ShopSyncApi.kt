package com.example.shopsync.data

import com.example.shopsync.BASE_URL
import com.example.shopsync.service.ShoppingList
import com.example.shopsync.service.ShoppingListItem
import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

class ShopSyncApi(private val httpClient: HttpClient) {

    suspend fun getShoppingLists(authToken: String): ApiResponse<List<ShoppingList>> {
        return try {
            val response = httpClient.get<List<ShoppingList>> {
                apiUrl("shoppingLists")
                headers {
                    append("Authorization", "Bearer $authToken")
                }
            }
            ApiResponse.Success(response)
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.Unauthorized) {
                ApiResponse.Unauthorized
            } else {
                ApiResponse.Failure(e.toString())
            }
        } catch (e: Exception) {
            ApiResponse.Failure(e.toString())
        }
    }

    suspend fun login(email: String, password: String): ApiResponse<LoginSignupResponse> {
        return try {
            val requestBody = buildJsonObject {
                put("email", email)
                put("password", password)
            }
            val response = httpClient.post<LoginSignupResponse> {
                apiUrl("login")
                contentType(ContentType.Application.Json)
                body = requestBody
            }
            ApiResponse.Success(response)
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.Unauthorized) {
                ApiResponse.Unauthorized
            } else {
                ApiResponse.Failure(e.toString())
            }
        } catch (e: Exception) {
            ApiResponse.Failure(e.toString())
        }
    }

    suspend fun signup(email: String, password: String): ApiResponse<LoginSignupResponse> {
        return try {
            val requestBody = buildJsonObject {
                put("firstName", "testFN")
                put("lastName", "testLN")
                put("email", email)
                put("password", password)
            }

            val response = httpClient.post<LoginSignupResponse> {
                apiUrl("signup")
                contentType(ContentType.Application.Json)
                body = requestBody
            }
            ApiResponse.Success(response)
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.Unauthorized) {
                ApiResponse.Unauthorized
            } else {
                ApiResponse.Failure(e.toString())
            }
        } catch (e: Exception) {
            ApiResponse.Failure(e.toString())
        }
    }

    suspend fun logout(): ApiResponse<LoginSignupResponse> {
        return try {
            val requestBody = buildJsonObject {
                put("token", "jawn")
            }
            val response = httpClient.post<LoginSignupResponse> {
                apiUrl("logout")
                contentType(ContentType.Application.Json)
                body = requestBody
            }
            ApiResponse.Success(response)
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.Unauthorized) {
                ApiResponse.Unauthorized
            } else {
                ApiResponse.Failure(e.toString())
            }
        } catch (e: Exception) {
            ApiResponse.Failure(e.toString())
        }
    }

    suspend fun createFirstItem(
        id: Int,
        authToken: String
    ): ApiResponse<SaveShoppingListResponse> {

        return try {
            val requestBody = buildJsonObject {
//                put("name", name)
//                put("userId", userId)
                put("listId", id)
                put("name", "untitled")
                put("quantity", 1)
                put("unit", "cnt")
                put("checked", false)
            }
            val response = httpClient.post<SaveShoppingListResponse> {
                apiUrl("shoppingListItems")
                contentType(ContentType.Application.Json)
                body = requestBody
                headers {
                    append("Authorization", "Bearer $authToken")
                }
            }
            ApiResponse.Success(response)
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.Unauthorized) {
                ApiResponse.Unauthorized
            } else {
                ApiResponse.Failure(e.toString())
            }
        } catch (e: Exception) {
            ApiResponse.Failure(e.toString())
        }

    }

    suspend fun saveShoppingList(
        name: String,
        userId: Int,
        authToken: String
    ): ApiResponse<SaveShoppingListResponse> {
        return try {
            val requestBody = buildJsonObject {
                put("name", name)
                put("userId", userId)
            }
            val response = httpClient.post<SaveShoppingListResponse> {
                apiUrl("shoppingLists")
                contentType(ContentType.Application.Json)
                body = requestBody
                headers {
                    append("Authorization", "Bearer $authToken")
                }
            }
            ApiResponse.Success(response)
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.Unauthorized) {
                ApiResponse.Unauthorized
            } else {
                ApiResponse.Failure(e.toString())
            }
        } catch (e: Exception) {
            ApiResponse.Failure(e.toString())
        }
    }

    suspend fun updateShoppingList(
        id: Int,
        name: String,
        userId: Int,
        authToken: String
    ): ApiResponse<String> {
        return try {
            val requestBody = buildJsonObject {
                put("id", id)
                put("name", name)
                put("userId", userId)
            }
            val response = httpClient.put<String> {
                println("idddddd: $id")
                apiUrl("shoppingLists/$id")
                contentType(ContentType.Application.Json)
                body = requestBody
                headers {
                    append("Authorization", "Bearer $authToken")
                }
            }
            if (response == "Shopping list updated successfully") {
                ApiResponse.Success("Shopping list updated successfully")
            } else {
                ApiResponse.Failure("Unexpected status code: ${response}")
            }
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.Unauthorized) {
                ApiResponse.Unauthorized
            } else {
                ApiResponse.Failure(e.toString())
            }
        } catch (e: Exception) {
            ApiResponse.Failure(e.toString())
        }
    }


    suspend fun getUsers(authToken: String): ApiResponse<List<UserListResponseItem>> {
        return try {
            val response = httpClient.get<List<UserListResponseItem>> {
                apiUrl("users")
                headers {
                    append("Authorization", "Bearer $authToken")
                }
            }
            ApiResponse.Success(response)
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.Unauthorized) {
                ApiResponse.Unauthorized
            } else {
                ApiResponse.Failure(e.toString())
            }
        } catch (e: Exception) {
            ApiResponse.Failure(e.toString())
        }
    }

    suspend fun getShoppingListItems(
        listId: Int,
        authToken: String
    ): ApiResponse<List<ShoppingListItem>> {
        return try {
            val response = httpClient.get<List<ShoppingListItem>> {
                apiUrl("shoppingListItems/list/$listId")
                headers {
                    append("Authorization", "Bearer $authToken")
                }
            }
            ApiResponse.Success(response)
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.Unauthorized) {
                ApiResponse.Unauthorized
            } else {
                ApiResponse.Failure(e.toString())
            }
        } catch (e: ClientRequestException) {
            ApiResponse.Failure(e.toString())
        }
    }

    suspend fun saveShoppingListItem(
        id: Int,
        name: String,
        quantity: Int,
        unit: String,
        listId: Int,
        checked: Boolean,
        authToken: String
    ): ApiResponse<SaveShoppingListResponse> {
        println(parametersOf())

        return try {
            val requestBody = buildJsonObject {
                put("id", id)
                put("name", name)
                put("quantity", quantity)
                put("unit", unit)
                put("listId", listId)
                put("checked", checked)
            }
            val response = httpClient.put<SaveShoppingListResponse> {
                apiUrl("shoppingListItems/$id")
                contentType(ContentType.Application.Json)
                body = requestBody
                headers {
                    append("Authorization", "Bearer $authToken")
                }
            }
            println("SAVE ITEM RESPONSE: $response")
            ApiResponse.Success(response)
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.Unauthorized) {
                ApiResponse.Unauthorized
            } else {
                ApiResponse.Failure(e.toString())
            }
        } catch (e: Exception) {
            ApiResponse.Failure(e.toString())
        }
    }

    private fun HttpRequestBuilder.apiUrl(path: String) {
        url("$BASE_URL/api/$path")
    }
}
