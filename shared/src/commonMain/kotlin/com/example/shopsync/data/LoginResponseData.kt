package com.example.shopsync.data

import kotlinx.serialization.Serializable

@Serializable
data class LoginResponseData(
    val token: String
)