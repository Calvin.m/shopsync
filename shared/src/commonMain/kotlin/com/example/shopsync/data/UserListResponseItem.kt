package com.example.shopsync.data

@kotlinx.serialization.Serializable
data class UserListResponseItem(
    val email: String,
    val firstName: String,
    val id: Int,
    val isAuthenticated: Boolean,
    val lastName: String,
    val password: String
)