package com.example.shopsync.data.repository

import com.example.shopsync.data.*
import com.example.shopsync.di.createHttpClient
//import com.example.shopsync.service.LoginSignupResponse
import com.example.shopsync.service.ShoppingList
import com.example.shopsync.service.ShoppingListItem

//class ShopSyncRepo(private val shopSyncApi: ShopSyncApi) {
//    suspend fun getShoppingLists(authToken: String): ApiResponse<List<ShoppingList>> {
//        return shopSyncApi.getShoppingLists(authToken)
//    }
//
//    suspend fun login(email: String, password: String): ApiResponse<LoginSignupResponse> {
//        return shopSyncApi.login(email, password)
//    }
//}

class ShopSyncRepo() {

    private val httpClient = createHttpClient()
    private val api = ShopSyncApi(httpClient)

    suspend fun getShoppingLists(authToken: String): ApiResponse<List<ShoppingList>> {
        return api.getShoppingLists(authToken)
    }

    suspend fun login(email: String, password: String): ApiResponse<LoginSignupResponse> {
        return api.login(email, password)
    }

    suspend fun signup(email: String, password: String): ApiResponse<LoginSignupResponse> {
        return api.signup(email, password)
    }

    suspend fun logout(): ApiResponse<LoginSignupResponse> {
        return api.logout()
    }

    suspend fun saveShoppingList(
        name: String,
        userId: Int,
        authToken: String
    ): ApiResponse<SaveShoppingListResponse> {
        return api.saveShoppingList(name, userId, authToken)
    }

    suspend fun updateShoppingList(
        id: Int,
        name: String,
        userId: Int,
        authToken: String
//    ): ApiResponse<String> {
    ): Boolean {
//    ) {
//        return api.updateShoppingList(id, name, userId, authToken)
        val result = api.updateShoppingList(id, name, userId, authToken)
        println("result: $result")
        println("result: $result")
        println("result: $result")
        println("result: $result")
        println("result: $result")
        return when (result) {
            is ApiResponse.Success -> true
            else -> false
        }
    }

    suspend fun getUsers(authToken: String): List<UserListResponseItem> {
//        return api.getUsers(authToken)
        val apiResponse = api.getUsers(authToken)
        val usersList = if (apiResponse is ApiResponse.Success) {
            apiResponse.data
        } else {
//            Log.e("USERS LIST:", "Failed to fetch users: $apiResponse")
            emptyList<UserListResponseItem>() // Return an empty list if the API call failed
        }
//        Log.e("USERS LIST:", "$usersList")
        return usersList
    }

    suspend fun createNewList(
        userId: Int,
        authToken: String
    ): Int {
        val apiResponse = api.saveShoppingList("untitled", userId, authToken)
        println("API RESPONSE in REPO: $apiResponse")
        return if (apiResponse is ApiResponse.Success) {
            apiResponse.data.id
        } else {
            -1
        }
    }

    suspend fun createFirstItem(listId: Int, authToken: String): Int {
//        val apiResponse = api.createFirstItem(listId, authToken)
        val apiResponse = api.createFirstItem(
//            name = "",
//            quantity = 1,
//            unit = "cnt",
//            listId = listId,
//            checked = false,
//            authToken
            id = listId,
            authToken
        )
        return if (apiResponse is ApiResponse.Success) {
            apiResponse.data.id
        } else {
            -1
        }
    }

    suspend fun getShoppingListItems(listId: Int, authToken: String): List<ShoppingListItem> {
        val apiResponse = api.getShoppingListItems(listId, authToken)
        val itemsList = if (apiResponse is ApiResponse.Success) {
            apiResponse.data
        } else {
            println("API RESPONSE: $apiResponse")
            println("API RESPONSE UNSUCCESSFUL")
            println("API RESPONSE UNSUCCESSFUL")
            println("API RESPONSE UNSUCCESSFUL")
            emptyList<ShoppingListItem>() // Return an empty list if the API call failed
        }
        return itemsList
    }

    //
    suspend fun saveShoppingListItem(
//    id: Int,
//    name: String,
//    quantity: Int,
//    unit: String,
//    listId: Int,
//    checked: Boolean
        shoppingListItem: ShoppingListItem,
        authToken: String
    ): ApiResponse<SaveShoppingListResponse> {
        val response = api.saveShoppingListItem(
            shoppingListItem.id,
            shoppingListItem.name,
            shoppingListItem.quantity,
            shoppingListItem.unit,
            shoppingListItem.listId,
            shoppingListItem.checked,
            authToken
        )
        println("RESPONSE in REPO: $response")
        println("RESPONSE in REPO: $response")
        println("RESPONSE in REPO: $response")
        println("RESPONSE in REPO: $response")
        println("RESPONSE in REPO: $response")
        println("RESPONSE in REPO: $response")
        println("RESPONSE in REPO: $response")
        println("RESPONSE in REPO: $response")
        println("RESPONSE in REPO: $response")
        println("RESPONSE in REPO: $response")
        return response
    }

}