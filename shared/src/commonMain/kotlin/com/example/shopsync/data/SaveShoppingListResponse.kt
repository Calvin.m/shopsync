package com.example.shopsync.data

import kotlinx.serialization.Serializable

@Serializable
data class SaveShoppingListResponse(
    val id: Int
)
