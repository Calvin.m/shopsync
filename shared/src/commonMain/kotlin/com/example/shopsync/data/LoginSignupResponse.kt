package com.example.shopsync.data

import kotlinx.serialization.Serializable

@Serializable
data class LoginSignupResponse(
    val data: LoginResponseData,
    val message: String,
    val status: String
)