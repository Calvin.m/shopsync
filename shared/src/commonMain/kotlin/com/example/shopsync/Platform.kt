package com.example.shopsync

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform