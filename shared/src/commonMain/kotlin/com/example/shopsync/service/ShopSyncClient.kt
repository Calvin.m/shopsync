package com.example.shopsync.service

import kotlinx.serialization.Serializable

interface ShoppingListClient {
    suspend fun getShoppingLists(): List<ShoppingList>
    suspend fun getShoppingList(id: Int): ShoppingList?
    suspend fun shareList(listId: Int, userEmail: String): Boolean
}

@Serializable
data class ShoppingList(
    val id: Int = 99,
    val name: String = "untitled",
    val userId: Int = 99,
    // todo - will eventually have animated expand/contract mechanism
    val items: List<ShoppingListItem> = emptyList()
)

@Serializable
data class ShoppingListItem(
    val id: Int = -1,
    val name: String = "",
    val quantity: Int = -1,
    val unit: String = "none",
    val listId: Int = -1,
    val checked: Boolean = false
)
